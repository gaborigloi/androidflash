package net.srcf.user.gi225.flash;

import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

// TODO move original svg icon to project folder from Pictures/icons
// TODO store icons in mipmap/
public class flashw extends View {

	private final Paint p;
	private boolean e=false;
	private int tx=0, ty=0;
	private boolean ex=false;
	private Camera cam;

	// FIXME doesn't do anything on motorola
	public flashw(Context context) {
		super(context);
		p = new Paint();
		p.setAntiAlias(true);
		ex = context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH);
		if (!ex)
			Toast.makeText(context, R.string.flash_not_supported, Toast.LENGTH_SHORT).show();
	}
	
	@Override
	public void onDraw(Canvas canvas)
	{
		canvas.drawColor(Color.BLACK);
		if (e) {
			if (ex) {
				canvas.drawColor(Color.BLACK);
				p.setStyle(Style.FILL);
				p.setColor(Color.WHITE);
				canvas.drawCircle(tx/2, ty/2, tx/5, p);
			} else {
				canvas.drawColor(Color.WHITE);
				p.setColor(Color.BLACK);
				p.setStyle(Style.STROKE);
				canvas.drawCircle(tx/2, ty/2, tx/5+tx/40, p);				
			}
		} else {
			canvas.drawColor(Color.BLACK);
			p.setStyle(Style.STROKE);
			p.setColor(Color.WHITE);
			canvas.drawCircle(tx/2, ty/2, tx/5+tx/40, p);
		}
				
	}
	
	@Override
	public void onSizeChanged(int w, int h, int oldw, int oldh)
	{
		p.setStrokeWidth(w/20);
		tx = w;
		ty = h;
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if (event.getAction()==MotionEvent.ACTION_DOWN) {
			float x = tx/2-event.getX();
			float y = ty/2-event.getY();
			if (x*x+y*y<=(tx/5)*(tx/5)) {
				e = !e;
				invalidate();
				if (ex) {
					if (e)
						flasha();
					else
						flashe();
				}
			}
		}
		return super.onTouchEvent(event);
	}
	
	@Override
	protected void onWindowVisibilityChanged(int visibility) {
		if (visibility==View.GONE||visibility==View.INVISIBLE)
			if (e) {
				e=false;
				invalidate();
				if (ex)
					flashe();
			}
		super.onWindowVisibilityChanged(visibility);
	}

	void flasha() {
		cam = getCameraInstance();  
		if (cam!=null) {
			Parameters p = cam.getParameters();
			p.setFlashMode(Parameters.FLASH_MODE_TORCH);
			cam.setParameters(p);
			cam.startPreview();
		} else
			Toast.makeText(getContext(), R.string.camera_not_available, Toast.LENGTH_SHORT).show();
	}
	
	void flashe() {
		if (cam!=null) {
			cam.stopPreview();
			cam.release();
		} else
			Toast.makeText(getContext(), R.string.camera_not_available, Toast.LENGTH_SHORT).show();
	}
	
	/** A safe way to get an instance of the Camera object. */
	public static Camera getCameraInstance(){
	    Camera c = null;
	    try {
	        c = Camera.open(); // attempt to get a Camera instance
	    }
	    catch (Exception e){
	        // Camera is not available (in use or does not exist)
	    }
	    return c; // returns null if camera is unavailable
	}
		
}
