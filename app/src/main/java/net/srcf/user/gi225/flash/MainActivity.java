package net.srcf.user.gi225.flash;

import android.os.Bundle;
import android.view.WindowManager.LayoutParams;
import android.app.Activity;


// TODO ? further decrease size of application

/* TODO
 * For example, if your application uses the Camera, the onPause() method is a good place to release it.

@Override
public void onPause() {
    super.onPause();  // Always call the superclass method first

    // Release the Camera because we don't need it when paused
    // and other activities might need to use it.
    if (mCamera != null) {
        mCamera.release()
        mCamera = null;
    }
}
 */

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().addFlags(LayoutParams.FLAG_KEEP_SCREEN_ON);
		setContentView(new flashw(this));
	}

}

